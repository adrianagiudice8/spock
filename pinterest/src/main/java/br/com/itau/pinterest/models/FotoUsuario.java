package br.com.itau.pinterest.models;

import java.time.LocalDate;
import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class FotoUsuario {

	@PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
	private String emailUsuario;

	@PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
	private LocalDate dataPostagemFoto;

	@NotNull
	private UUID idFoto;

	@NotBlank
	private String linkFoto;

	public String getEmailUsuario() {
		return emailUsuario;
	}

	public void setEmailUsuario(String emailUsuario) {
		this.emailUsuario = emailUsuario;
	}

	public LocalDate getDataPostagemFoto() {
		return dataPostagemFoto;
	}

	public void setDataPostagemFoto(LocalDate dataPostagemFoto) {
		this.dataPostagemFoto = dataPostagemFoto;
	}

	public UUID getIdFoto() {
		return idFoto;
	}

	public void setIdFoto(UUID idFoto) {
		this.idFoto = idFoto;
	}

	public String getLinkFoto() {
		return linkFoto;
	}

	public void setLinkFoto(String linkFoto) {
		this.linkFoto = linkFoto;
	}

}
