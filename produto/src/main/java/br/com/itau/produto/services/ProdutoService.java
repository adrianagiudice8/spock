package br.com.itau.produto.services;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.itau.produto.models.Produto;
import br.com.itau.produto.repositories.ProdutoRepository;


@Service
public class ProdutoService {
	@Autowired
	ProdutoRepository produtoRepository;
	
	RestTemplate restTemplate = new RestTemplate();

	
	public Iterable<Produto> listar(){
		//Produto produto = restTemplate.getForObject("http://localhost:8081/produto/" + produto.getId(), Produto.class);
		//produto.setId(id);
		
		return produtoRepository.findAll();
	}
	
	//public Iterable<Produto> obterProdutoporId(int id) {
	//	return produtoRepository.findById(id);
	//}
	
	public Optional<Produto> buscar(int id) {
		return produtoRepository.findById(id);
	}

	
	@PostConstruct
	public void inicializarBase() {
		Produto produto1 = new Produto();
		produto1.setNome("Poupança");
		produto1.setRendimento(0.005);
		
		Produto produto2 = new Produto();
		produto2.setNome("CDB");
		produto2.setRendimento(0.0075);
		
		Produto produto3 = new Produto();
		produto3.setNome("Fundos");
		produto3.setRendimento(0.008);
		
		produtoRepository.save(produto1);
		produtoRepository.save(produto2);
		produtoRepository.save(produto3);
	}
}
