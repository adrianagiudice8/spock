package br.com.itau.produto.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.produto.models.Transacao;

public interface TransacaoRepository extends CrudRepository<Transacao, Integer> {

}
