package br.com.itau.produto.viewobjects;

import br.com.itau.produto.models.Aplicacao;
import br.com.itau.produto.models.Cliente;
import br.com.itau.produto.models.Produto;
import br.com.itau.produto.models.Transacao;

public class Investimento {
	private Cliente cliente;
	
	private Produto produto;
	
	private Transacao transacao;
	
	private Aplicacao aplicacao;

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Transacao getTransacao() {
		return transacao;
	}

	public void setTransacao(Transacao transacao) {
		this.transacao = transacao;
	}
	
	public void setAplicacao(Aplicacao aplicacao) {
		this.aplicacao = aplicacao;
	}
	
	public Aplicacao getAplicacao() {
		return aplicacao;
	}
}
