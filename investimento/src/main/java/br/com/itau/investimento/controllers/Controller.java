package br.com.itau.investimento.controllers;

import java.util.List;

import javax.annotation.Generated;

import org.springframework.beans.factory.annotation.Autowired;
import br.com.itau.investimento.Investimento;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.service.ContratoService;

import br.com.itau.investimento.Investimento;
import br.com.itau.investimento.service.ContratoService;

@RestController
@RequestMapping("/investimento")
public class Controller {
	@Autowired
	ContratoService contratoService;

	@GetMapping
	public List<Investimento> listarInvestimento() {
		return ContratoService.getInvestimento();
	}

	@GetMapping("/{id}")
	public List<Investimento> listarInvestimentoPorId(@PathVariable int id) {
	//	List<Investimento> investimentos = ContratoService.getInvestimento();
		
		return ContratoService.getInvestimento();
		
	}
	
	@GetMapping("/{id}/{valor}")
	public List<Investimento> listarInvestimentoPorIdeValor(@PathVariable int id,@PathVariable double valor) {
		//List<Investimento> listarInvestimento();
		ContratoService contrato = new ContratoService();
		contrato.Calcular(valor);
		return ContratoService.getInvestimento();
		
	}

	@PostMapping
	public boolean inserirInvestimento(@RequestBody Investimento investimento) {
		return ContratoService.inserirInvestimento(investimento);
	}
}
