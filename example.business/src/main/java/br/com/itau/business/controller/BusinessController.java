package br.com.itau.business.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/business")
public class BusinessController {

	@GetMapping
	public ResponseEntity myBusinessMethod() {

		System.out.println("funcionou");

		return ResponseEntity.status(200).build();
	}
}
