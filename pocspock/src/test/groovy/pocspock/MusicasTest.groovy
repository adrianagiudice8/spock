package pocspock

import spock.lang.Specification

class MusicasTest extends Specification {
	def 'deve cantar a musica certa'(){
		given: 'receber o nome da moça'
		def mina = 'Estella'
		
		when:'cante a musica'
		def musica = "o nome dela é $mina"

		then: 'a musica deve ser sobre a Jenifer'
		musica == "o nome dela é Estella"		
	}
	
}
