package pocspock

class Pessoa {

	String nome;
	int idade;

	static void main(args) {

		println("Hello groovy");

		def pessoinha = new Pessoa(nome: 'Yoshi', idade: 37);

		println("O nome dele é ${pessoinha.nome}");

		pessoinha.setNome('José');

		println ("O nome dele é ${pessoinha.nome}")
		println ("""
			Seu amor me pegou
			Que tiro foi esse?
		""");

		def cores = ['azul', 'verde', 'branco']
		println(cores[0]);
		println(cores[-1]);

		println(cores.first());
		println(cores.last());

		cores.each { cor ->
			println("cor: ${cor}")
		}

		//exiba uma palavra qualquer 10x
		10.times { println("abacate") }



		//ele entende a variavel como vc quiser...no caso abaixo entendo como boolean

		if (cores) {
			println("cores tem algo")
		}

		def filhos =5
		if (filhos) {
			println("tem filhos")
		}

		filhos =0;
		if (filhos) {
			println("tem filhos");
		}
		// para nao fazer nova chamada quando der null point e nap dar null point
		List bairros = null;
		println(bairros?.first());
		
		bairros = ['cidade tiradentes', 'guainazes'];
		println(bairros?.first());
	}
}

