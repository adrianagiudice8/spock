package pocspock

import br.com.itau.pocspock.PorteiroEscandalo
import spock.lang.Specification

class PorteiroEscandaloTest extends Specification {

	def 'deve liberar caso seja maior de idade'(){
		given: 'receber a idade do cliente'
		def idade1 = 21
		def idade2 = 17

		when: 'verificar a idade'
		def porteiro = new PorteiroEscandalo()
		def verificacao1 = porteiro.podeEntrar(idade1)
		def verificacao2 = porteiro.podeEntrar(idade2)

		then: 'deve liberar só para idade a partir de 18'
		verificacao1
		!verificacao2
	}
	def 'deve calcular o desconto conforme a idade'() {
		expect:'O desconto deve estar correto conforme a idade'
		new PorteiroEscandalo().getDesconto(idade) == desconto

		where:
		idade | desconto
		18	  | 90
		23	  | 0
		25	  | 0
		26	  | 10
		39	  | 10
		42	  | 20
		50	  | 20
	}
}