package br.com.itau.pocspock;

public class PorteiroEscandalo {
	
	public boolean podeEntrar(int idade) {
		return idade >= 18;
	}
	
	public double getDesconto(int idade) {
		if (idade <= 25) {
			return 0;
		}else if (idade <= 40) {
			return 10;
		}else {
			return 20;
		}
	}

}
