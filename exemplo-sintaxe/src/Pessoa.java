
public class Pessoa {
	private int idade;
	private String nome;
	private String cpf;
	
	public Pessoa(String nome, int idade) {
		this.nome = nome;
		this.idade = idade;
	}
	public Pessoa(String nome) {
		this.nome = nome;
	}
	public String toString() {
		return nome + " - " + idade;
	}
}
