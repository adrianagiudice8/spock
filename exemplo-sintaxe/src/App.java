
public class App {
	public static void main(String[]args) {
		int umNumero = 10;
		double numeroDecimal = 10.0;
		boolean eVerdade = false;
		char umaLetra = 'a';
		String umaFrase = "";
		
		//Instanciando um objeto
		Pessoa pessoa = new Pessoa("João", 30);
		//pessoa.nome = "João";
		//pessoa.idade = 30;
		System.out.println(pessoa);
		
		//vetores
		//int[] numeros = new int [3];
		int[] numeros = {1, 5, 9, 20};
		
		//Estrutura de controle
		if (umNumero > 10) {
				System.out.println("Maior que dez");
		}else {
			System.out.println("Menor que dez");
		}
		if (umNumero == 10 && umNumero < 20) {
			System.out.println("Igual que 10 e menor que 20");
		}
		if (umNumero == 10 || umNumero < 20) {
			System.out.println("Igual que 10 ou menor que 20");
		}
		for (int i = 0; i < 10; i++) {
			System.out.println(i);
		}
		for (int numero : numeros) {
			System.out.println(numero);
		}
		while (eVerdade) {
			System.out.println(eVerdade);
			eVerdade = false;
		}
		
		do {
			System.out.println(eVerdade);
		} while (eVerdade);
		
		//operadores
		int soma = umNumero + umNumero;
		int subtracao = umNumero - umNumero;
		int multiplicacao = umNumero * umNumero;
		int divisao = umNumero / umNumero;
		
		umNumero++;
		umNumero--;
		
		umNumero += 10;
		
		//Concatenação
		System.out.println(umaFrase + "....");
		
	}		
}
