import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public enum Niquel {
	BANANA("Banana", 10), FRAMBOESA("Framboesa", 50), MOEDA("Moeda", 100), SETE("Sete", 300);

	private String nome;
	private int valor;

	private Niquel(String nome, int valor) {
		this.nome = nome;
		this.valor = valor;
	}

	public double getValor() {
		return valor;
	}

	public String getNomeString() {
		return nome;
	}
}
